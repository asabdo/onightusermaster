package com.android.wanetusermaster.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.wanetusermaster.R;
import com.android.wanetusermaster.models.Employee;
import com.android.wanetusermaster.models.GeneralData;
import com.android.wanetusermaster.models.OrderLocation;
import com.android.wanetusermaster.models.Orders;
import com.squareup.picasso.Picasso;


/**
 * Created by Samer AlShurafa on 2/9/2018.
 */


public class OrdersViewHolder extends RecyclerView.ViewHolder {


    private Context context;
    private ImageView cardUserImage;
    private TextView driverName, carModel, orderDate, sourceName, destinationName;


    public OrdersViewHolder(View itemView) {
        super(itemView);

        this.context = itemView.getContext();

        cardUserImage = itemView.findViewById(R.id.cardUserImage);
        driverName = itemView.findViewById(R.id.driverName);
        carModel = itemView.findViewById(R.id.carModel);
        orderDate = itemView.findViewById(R.id.orderDate);
        sourceName = itemView.findViewById(R.id.sourceName);
        destinationName = itemView.findViewById(R.id.destinationName);
    }


    public void updateUI(Orders orders) {
        Employee driver = orders.getDriverModel();

        driverName.setText(driver.getName());
        carModel.setText(driver.getCar_model());
        orderDate.setText(GeneralData.getTimeDate(orders.getService_time()));

        OrderLocation orderLocation = orders.getSourceLocation();
        sourceName.setText(orderLocation.getName());

        orderLocation = orders.getDestinationLocation();
        destinationName.setText(orderLocation.getName());


        if(!driver.getAvatar().isEmpty() && !driver.getAvatar().equalsIgnoreCase(GeneralData.DB_AVATAR_CHILD_DEFAULT)) {

            Picasso.with(context)
                    .load(driver.getAvatar())
                    .placeholder(R.drawable.profile_user)
                    .error(R.drawable.logo_orange)
                    .into(cardUserImage);

        }

    }

}



