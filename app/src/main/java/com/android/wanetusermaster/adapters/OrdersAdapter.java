package com.android.wanetusermaster.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.wanetusermaster.R;
import com.android.wanetusermaster.holders.OrdersViewHolder;
import com.android.wanetusermaster.models.GeneralData;
import com.android.wanetusermaster.models.Orders;
import com.android.wanetusermaster.models.OrdersListener;

import java.util.ArrayList;

/**
 * Created by Samer AlShurafa on 2/9/2018.
 */


public class OrdersAdapter extends RecyclerView.Adapter<OrdersViewHolder>  {

    private ArrayList<Orders> ordersList;
    private OrdersListener ordersListener;


    public OrdersAdapter(ArrayList<Orders> ordersList, Context context) {
        this.ordersList = ordersList;
        this.ordersListener = (OrdersListener) context;
    }


    @Override
    public OrdersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View card_order = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_order_layout, parent, false);

        return new OrdersViewHolder(card_order);
    }


    @Override
    public void onBindViewHolder(OrdersViewHolder ordersViewHolder, @SuppressLint("RecyclerView") final int position) {
        final Orders orders = ordersList.get(position);
        ordersViewHolder.updateUI(orders);

        ordersViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ordersListener.onOrderSelected(orders, GeneralData.ORDERS_TO_DETAILS);

            }
        });

    }


    @Override
    public int getItemCount() {
        return ordersList.size();
    }


}


