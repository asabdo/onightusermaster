package com.android.wanetusermaster.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.wanetusermaster.R;
import com.android.wanetusermaster.models.GeneralData;


public class TermsOfUseActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_terms_of_use);


        TextView termsContent = findViewById(R.id.termsContent);
        termsContent.setText(Html.fromHtml(
                "1. " + getResources().getString(R.string.terms_content_1)
                        + "<br><br>"
                        + "2. " + getResources().getString(R.string.terms_content_2)
                        + "<br><br>"
                        + "3. " + getResources().getString(R.string.terms_content_3)
        ));


        Button acceptButton = findViewById(R.id.acceptButton);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Change the Shared References ..
                SharedPreferences sharedPreferences = getSharedPreferences(GeneralData.FILE_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(GeneralData.FIRST_TIME_KEY, "no");
                editor.apply();

                // Go to Control Activity ..
                startActivity(new Intent(getApplicationContext(), ControlActivity.class));
            }
        });



    }
}
