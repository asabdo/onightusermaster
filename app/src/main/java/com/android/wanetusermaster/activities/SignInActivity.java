package com.android.wanetusermaster.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.wanetusermaster.models.ExceptionHandling;
import com.android.wanetusermaster.models.GeneralData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import com.android.wanetusermaster.R;


public class SignInActivity extends AppCompatActivity {


    EditText phoneNumber, verificationCode;
    Spinner phoneCode;
    Button sendCode, resendCode;
    LinearLayout phoneNumberLayout, verificationCodeLayout;


    private FirebaseAuth mAuth;
    //private DatabaseReference databaseReference = null;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private String userID;
    private String phone_code = "";
    private String mobile_number;
    private String phone_number = "";

    private ProgressDialog verificationProgressDialog = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_sign_in);

        phoneNumber = findViewById(R.id.phoneNumber);
        verificationCode = findViewById(R.id.verificationCode);
        phoneCode = findViewById(R.id.phoneCode);
        sendCode = findViewById(R.id.sendCode);
        resendCode = findViewById(R.id.resendCode);
        phoneNumberLayout = findViewById(R.id.phoneNumberLayout);
        verificationCodeLayout = findViewById(R.id.verificationCodeLayout);


        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.country_arrays, R.layout.spinner_layout);
        phoneCode.setAdapter(adapter);


        // Show the keyboard to phone number edit after login to this activity
        phoneNumber.requestFocus();

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            //imm.showSoftInput(phoneNumber, 0);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }


        phoneCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getItemAtPosition(position).toString()
                        .equalsIgnoreCase(getResources().getString(R.string.country_saudi_arabia))) {
                    phone_code = "+966";
                }
                //else
                //    phone_code = "+970";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        sendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validatePhoneNumber()) {
                    // Hide the keyboard ..
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    if(imm != null) {
                        imm.hideSoftInputFromWindow(phoneNumber.getWindowToken(), 0);
                    }
                    confirmDialog();
                }
            }
        });


        resendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // re-send the code to same phone number
                resendVerificationCode(mResendToken);
            }
        });


        // If user enter 6 digits in verificationCode edit text ==> check it ..
        verificationCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 6) {
                    showVerificationProgressDialog(getResources().getString(R.string.check_verification_code_title),
                            getResources().getString(R.string.check_verification_code_description));
                    verifyPhoneNumberWithCode(mVerificationId, verificationCode.getText().toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        mAuth = FirebaseAuth.getInstance();

        //databaseReference = FirebaseDatabase.getInstance().getReference(GeneralData.DB_USERS_CHILD);



        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                if(verificationProgressDialog.isShowing()) {
                    verificationProgressDialog.hide();
                    verificationProgressDialog.dismiss();
                }
                phoneNumberLayout.setVisibility(View.GONE);
                verificationCodeLayout.setVisibility(View.VISIBLE);

                // Show the keyboard
                verificationCode.requestFocus();
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm != null) {
                    //imm.showSoftInput(verificationCode, 0);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
            }


            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w("SignInActivity", "onVerificationFailed", e);
                if(verificationProgressDialog.isShowing()) {
                    verificationProgressDialog.hide();
                    verificationProgressDialog.dismiss();
                }
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_verification_failed),
                        Toast.LENGTH_LONG).show();

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    phoneNumber.setError(getResources().getString(R.string.error_incorrect_phone_number));
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    ExceptionHandling.exceptionOccurred("Sign In Activity", "SMS quota has been exceeded " + e.getMessage());
                }

            }


            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d("SignInActivity", "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = forceResendingToken;

                // [START_EXCLUDE]
                // Update UI
                //updateUI(STATE_CODE_SENT);
                // [END_EXCLUDE]
            }


        }; // End initialize mCallbacks




    } // end onCreate function




    @Override
    protected void onPause() {
        super.onPause();
        // Hide the keyboard before go to Main activity ..
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
        }
    }



    private void confirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);

        TextView title = new TextView(this);
        title.setText(getResources().getString(R.string.please_confirm_title));
        title.setTextSize(20);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_red));

        TextView messageText = new TextView(this);
        messageText.setText(Html.fromHtml("<br>"
                + getResources().getString(R.string.is_this_number_for_yours)
                + "<br> <br>"
                + mobile_number
        ));
        messageText.setTextSize(17);
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        messageText.setTextColor(getResources().getColor(R.color.color_main_orange));

        builder
                .setCustomTitle(title)
                .setView(messageText)
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.answer_yes),  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        startPhoneNumberVerification();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.answer_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })

                .show();
    }




    private void showVerificationProgressDialog(String dialogTitle, String dialogDescription) {

        verificationProgressDialog = new ProgressDialog(this, R.style.AlertDialogTheme);

        TextView title = new TextView(this);
        title.setText(Html.fromHtml("<br>" + dialogTitle + "<br><br>" + dialogDescription));
        title.setTextSize(17);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_main_orange));

        verificationProgressDialog.setCustomTitle(title);
        verificationProgressDialog.setCancelable(false);

        verificationProgressDialog.show();
    }







    private boolean validatePhoneNumber() {

        //phone_number = phone_code + phoneNumber.getText().toString().trim();
        phone_number = phoneNumber.getText().toString().trim();

        if (TextUtils.isEmpty(phone_number)) {
            phoneNumber.setError(getResources().getString(R.string.error_enter_phone_number));
            return false;
        } else if(phone_number.length() < 10) {
            phoneNumber.setError(getResources().getString(R.string.error_incorrect_phone_number));
            return false;
        }

        if(phone_number.startsWith("+966")) { // mostly could not
            mobile_number = phone_number;
            return true;
        } else if(phone_number.startsWith("00"))
            phone_number = phone_number.replaceFirst("00", "+"); // replace 00 by + in the first if exist ..
        else if(phone_number.startsWith("0")) {
            phone_number = phone_number.substring(1);
            phone_number = "+966" + phone_number;
        } else if(phone_number.startsWith("+970") || phone_number.startsWith("970")) { // handle dummy number for test
            phone_number = phone_number.replace("+", "");
            phone_number = phone_number.replaceFirst("970", "+970");
        } else return false;

        mobile_number = phone_number;

        return true;
    }




    private void startPhoneNumberVerification() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile_number,                 // Phone number to verify
                60,                       // Timeout duration
                TimeUnit.SECONDS,            // Unit of timeout
                this,                 // Activity (for callback binding)
                mCallbacks);                 // OnVerificationStateChangedCallbacks
        showVerificationProgressDialog(getResources().getString(R.string.send_verification_code_title),
                getResources().getString(R.string.send_verification_code_description));
        //mVerificationInProgress = true;
    }





    private void resendVerificationCode(PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile_number,            // Phone number to verify
                60,                  // Timeout duration
                TimeUnit.SECONDS,       // Unit of timeout
                this,            // Activity (for callback binding)
                mCallbacks,             // OnVerificationStateChangedCallbacks
                token);                 // ForceResendingToken from callbacks
        showVerificationProgressDialog(getResources().getString(R.string.send_verification_code_title),
                getResources().getString(R.string.send_verification_code_description));
    }






    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }





    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {

        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(verificationProgressDialog.isShowing()) {
                    verificationProgressDialog.hide();
                    verificationProgressDialog.dismiss();
                }

                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("SignInActivity", "signInWithCredential:success");

                    if(task.getResult() != null) {
                        FirebaseUser user = task.getResult().getUser();
                        userID = user.getUid();
                    }

                    SharedPreferences sharedPreferences = getSharedPreferences(GeneralData.FILE_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(GeneralData.SIGN_IN_KEY, "yes");
                    editor.putString(GeneralData.DB_ID_CHILD, userID); // user id for new instance if customer login another time
                    editor.putString(GeneralData.DB_MOBILE_NO_CHILD, mobile_number);
                    editor.apply();



                    Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                    startActivity(intent);

                } else {
                    // Sign in failed, display a message and update the UI
                    Log.w("SignInActivity", "signInWithCredential:failure", task.getException());
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        verificationCode.setError(getResources().getString(R.string.error_incorrect_verification_code));
                    }

                } // end else

            }
        });
    } // End signInWithPhoneAuthCredential






}




