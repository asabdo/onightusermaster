package com.android.wanetusermaster.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.wanetusermaster.models.ExceptionHandling;
import com.android.wanetusermaster.models.GeneralData;

import com.android.wanetusermaster.R;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class ControlActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_control);



        // ToDo -For test
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(this, new String[]
                            {ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                    GeneralData.LOCATION_PERMISSION_CODE);

        } else {

            Intent intent;

            // check if first run or not ..
            SharedPreferences sharedPreferences = getSharedPreferences(GeneralData.FILE_NAME, MODE_PRIVATE);

            if(sharedPreferences != null && sharedPreferences.getString(GeneralData.FIRST_TIME_KEY, GeneralData.DEFAULT_VALUE)
                    .equalsIgnoreCase("no")) {
                // Not first time

                if(sharedPreferences.getString(GeneralData.SIGN_IN_KEY, GeneralData.DEFAULT_VALUE)
                        .equalsIgnoreCase("yes")) {

                    // User is signed in .. so ==> Go to Maps Activity ..
                    intent = new Intent(ControlActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                } else {

                    // Not signed in .. so ==> Go to SignInActivity
                    intent = new Intent(ControlActivity.this, SignInActivity.class);
                    startActivity(intent);
                    finish();

                }


            } else {

                try {
                    if(sharedPreferences != null) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(GeneralData.FIRST_TIME_KEY, "yes");
                        editor.apply();
                    }

                    intent = new Intent(ControlActivity.this, ViewPagerActivity.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    ExceptionHandling.exceptionOccurred("Control Activity", "Exception " + e.getMessage());
                }

            } // end Shared Preferences else
        }


    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case GeneralData.LOCATION_PERMISSION_CODE:
                // ToDo remove this before lunch
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                break;
        }
    }

}
