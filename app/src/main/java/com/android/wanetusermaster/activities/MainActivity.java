package com.android.wanetusermaster.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.wanetusermaster.R;
import com.android.wanetusermaster.fragments.OrdersTimeLineFragment;
import com.android.wanetusermaster.fragments.MapsFragment;
import com.android.wanetusermaster.fragments.OrdersFragment;
import com.android.wanetusermaster.fragments.OrdersDetailsFragment;
import com.android.wanetusermaster.fragments.ProfileFragment;
import com.android.wanetusermaster.fragments.TermsOfUseFragment;
import com.android.wanetusermaster.models.ExceptionHandling;
import com.android.wanetusermaster.models.GeneralData;
import com.android.wanetusermaster.models.LoadFragmentListener;
import com.android.wanetusermaster.models.Orders;
import com.android.wanetusermaster.models.OrdersListener;
import com.android.wanetusermaster.models.User;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Objects;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OrdersListener, LoadFragmentListener {


    NavigationView navigationView;
    LinearLayout profileLayout;
    CircularImageView profileImage;
    TextView profileName, profileMobileNumber;
    DrawerLayout drawer;

    String userAvatar = "", currentAvatar = "";
    private boolean ifFirstTime = true;

    private SharedPreferences sharedPreferences = null;
    private SharedPreferences.Editor editor = null;

    private DatabaseReference databaseReference = null;
    //private FirebaseUser currentUser = null;
    private ValueEventListener valueEventListener = null;

    private MapsFragment mapsFragment = null;
    private OrdersFragment ordersFragment = null;
    private TermsOfUseFragment termsOfUseFragment = null;
    private ProfileFragment profileFragment = null;
    private OrdersDetailsFragment ordersDetailsFragment = null;
    private OrdersTimeLineFragment ordersTimeLineFragment = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        View headerView = navigationView.getHeaderView(0);

        final Menu menu = navigationView.getMenu();
        MenuItem menu_login = menu.findItem(R.id.menu_login);

        if (Objects.equals(sharedPreferences.getString(GeneralData.SIGN_IN_KEY, GeneralData.DEFAULT_VALUE), "yes")) {
            menu_login.setTitle(getResources().getString(R.string.menu_logout));
        } else {
            menu_login.setTitle(getResources().getString(R.string.menu_login));
        }


        profileLayout = headerView.findViewById(R.id.profileLayout);
        profileImage = headerView.findViewById(R.id.profileImage);
        profileName = headerView.findViewById(R.id.profileName);
        profileMobileNumber = headerView.findViewById(R.id.profilePhoneNumber);

        /*TextView menuLogOut = findViewById(R.id.menu_log_out);

        menuLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPreferences != null) {
                    editor = sharedPreferences.edit();
                    editor.putString(GeneralData.SIGN_IN_KEY, "no");
                    editor.apply();
                }

                FirebaseAuth.getInstance().signOut();

                startActivity(new Intent(MainActivity.this, SignInActivity.class));
            }
        });*/

        sharedPreferences = getSharedPreferences(GeneralData.FILE_NAME, MODE_PRIVATE);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if(currentUser != null) {

            profileMobileNumber.setText(currentUser.getPhoneNumber());

            databaseReference = FirebaseDatabase.getInstance().getReference(GeneralData.DB_USERS_CHILD)
                    .child(currentUser.getUid());

        }


        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(profileFragment, "ProfileFragment");
                drawer.closeDrawer(GravityCompat.START);
            }
        });


        mapsFragment = MapsFragment.newInstance();
        ordersFragment = OrdersFragment.newInstance();
        termsOfUseFragment = TermsOfUseFragment.newInstance();
        profileFragment = ProfileFragment.newInstance();
        ordersDetailsFragment = OrdersDetailsFragment.newInstance();
        ordersTimeLineFragment = OrdersTimeLineFragment.newInstance();

        loadFragment(mapsFragment, "MapsFragment");
        loadFragment(ordersFragment, "OrdersFragment");
        loadFragment(termsOfUseFragment, "TermsOfUseFragment");
        loadFragment(profileFragment, "ProfileFragment");
        loadFragment(ordersDetailsFragment, "OrdersDetailsFragment");
        loadFragment(ordersTimeLineFragment, "OrdersTimeLineFragment");

    }


    @Override
    protected void onStart() {
        super.onStart();

        if(ifFirstTime) {
            loadFragment(mapsFragment, "MapsFragment");
            ifFirstTime = false;
        }

        updateProfileImage();

    } // end onStart




    private void updateProfileImage() {

        valueEventListener = databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                if(user != null && user.getAvatar() != null &&
                        !user.getAvatar().equalsIgnoreCase(GeneralData.DB_AVATAR_CHILD_DEFAULT)) {

                    if(user.getName() != null && user.getEmail() != null && user.getAvatar() != null && sharedPreferences != null) {
                        // If remove the app and reinstall it, check the shared ..
                        String name = sharedPreferences.getString(GeneralData.DB_NAME_CHILD, GeneralData.DEFAULT_VALUE);
                        String email = sharedPreferences.getString(GeneralData.DB_EMAIL_CHILD, GeneralData.DEFAULT_VALUE);
                        String avatar = sharedPreferences.getString(GeneralData.DB_AVATAR_CHILD,
                                GeneralData.DB_AVATAR_CHILD_DEFAULT);

                        editor = sharedPreferences.edit();

                        if(name.equalsIgnoreCase(GeneralData.DEFAULT_VALUE))
                            editor.putString(GeneralData.DB_NAME_CHILD, user.getName());
                        if(email.equalsIgnoreCase(GeneralData.DEFAULT_VALUE))
                            editor.putString(GeneralData.DB_EMAIL_CHILD, user.getEmail());
                        if(avatar.equalsIgnoreCase(GeneralData.DB_AVATAR_CHILD_DEFAULT))
                            editor.putString(GeneralData.DB_AVATAR_CHILD, user.getAvatar());

                        editor.apply();

                        currentAvatar = user.getAvatar();
                        if(!currentAvatar.equalsIgnoreCase(userAvatar)) {

                            userAvatar = currentAvatar;
                            Picasso.with(getApplicationContext())
                                    .load(currentAvatar)
                                    .placeholder(R.drawable.profile_user)
                                    .error(R.drawable.logo_orange)
                                    .into(profileImage);
                        }

                    }
                }
            } // en onDataChange

            @Override
            public void onCancelled(DatabaseError databaseError) { }

        });

    } // end update profile image



    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (getFragmentManager().getBackStackEntryCount() > 1) {
                getFragmentManager().popBackStack();
            }

            /*
            int fragments = getSupportFragmentManager().getBackStackEntryCount();

            if (fragments == 1) {
                finish();
            }
            else {
                if (getFragmentManager().getBackStackEntryCount() > 1) {
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
            */
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.profile, menu);
        return false; // true
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();

        databaseReference.removeEventListener(valueEventListener);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        boolean ifLogin = false;

        if (Objects.equals(sharedPreferences.getString(GeneralData.SIGN_IN_KEY, GeneralData.DEFAULT_VALUE), "yes")) {
            ifLogin = true;
        }

        if (id == R.id.menu_main_maps) {
            updateProfileImage();
            loadFragment(mapsFragment, "MapsFragment");
        } else if (id == R.id.menu_my_orders) {
            loadFragment(ordersFragment, "OrdersFragment");
        } else if (id == R.id.menu_terms_of_use) {
            loadFragment(termsOfUseFragment, "TermsOfUseFragment");
        } else if (id == R.id.menu_login) {
            if (ifLogin) {
                if (sharedPreferences != null) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(GeneralData.SIGN_IN_KEY, "no");
                    //editor.putString(AppConstants.DB_VERIFIED_CHILD, "no");
                    editor.remove(GeneralData.DB_ID_CHILD);
                    editor.remove(GeneralData.DB_MOBILE_NO_CHILD);
                    editor.apply();
                }

                //FirebaseAuth.getInstance().signOut();
                item.setTitle(getResources().getString(R.string.menu_login));
            } else {
                startActivity(new Intent(MainActivity.this, SignInActivity.class));
            }
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




    @Override
    public void onOrderSelected(Orders orders, String source) {

        if(source.equalsIgnoreCase(GeneralData.ORDERS_TO_DETAILS)) {
            ordersDetailsFragment.setOrders(orders);
            loadFragment(ordersDetailsFragment, "OrdersDetailsFragment");
        } else if(source.equalsIgnoreCase(GeneralData.DETAILS_TO_TIME_LINE)) {
            ordersTimeLineFragment.setOrders(orders);
            loadFragment(ordersTimeLineFragment, "OrdersTimeLineFragment");
        } else if(source.equalsIgnoreCase(GeneralData.TIME_LINE_TO_DETAILS)) {
            ordersDetailsFragment.setOrders(orders);
            loadFragment(ordersDetailsFragment, "OrdersDetailsFragment");
        }
    }



    @Override
    public void loadFragment(String fragmentName) {
        if(fragmentName.equalsIgnoreCase("MapsFragment")) {
            loadFragment(mapsFragment, "MapsFragment");
        } else if(fragmentName.equalsIgnoreCase("OrdersFragment")) {
            loadFragment(ordersFragment, "OrdersFragment");
        } else if(fragmentName.equalsIgnoreCase("ProfileFragment")) {
            loadFragment(profileFragment, "ProfileFragment");
        }
    }




    @SuppressLint("NewApi")
    private void loadFragment (Fragment fragment, String fragmentName) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(fragmentName);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();


        if (!isFinishing() && !isDestroyed()) {
            if (currentFragment != null) {
                fragmentTransaction.show(fragment);
            } else {
                try {
                    fragmentTransaction.add(R.id.fragment_container, fragment, fragmentName);
                } catch (Exception ex) {
                    ExceptionHandling.exceptionOccurred("MainActivity", "Exception: " + ex.getMessage());
                }
            }

            switch (fragmentName) {
                case "MapsFragment":
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "OrdersFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "TermsOfUseFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "ProfileFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "OrdersDetailsFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "OrdersTimeLineFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    break;
            }

            fragmentTransaction.addToBackStack(fragmentName).commit();

        }
    }


}
