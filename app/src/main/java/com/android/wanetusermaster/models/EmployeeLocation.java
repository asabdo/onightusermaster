package com.android.wanetusermaster.models;

/**
 * Created by Samer AlShurafa on 2/7/2018.
 */

public class EmployeeLocation {

    private double latitude;
    private double longitude;

    public EmployeeLocation() { }

    public EmployeeLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

}
