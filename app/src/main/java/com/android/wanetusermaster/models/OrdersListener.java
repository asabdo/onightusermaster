package com.android.wanetusermaster.models;

/**
 * Created by Samer AlShurafa on 2/11/2018.
 */

public interface OrdersListener {

    void onOrderSelected(Orders orders, String source);

}
