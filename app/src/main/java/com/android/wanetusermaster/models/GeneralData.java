package com.android.wanetusermaster.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.DateFormat;

import com.android.wanetusermaster.activities.ControlActivity;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Samer AlShurafa on 1/20/2018.
 */

public class GeneralData {

    public final static String FILE_NAME = "MyDate";

    public final static String FIRST_TIME_KEY = "first_time";
    public final static String SIGN_IN_KEY = "sign_in";
    public final static String DEFAULT_VALUE = "N/A";


    public final static String DB_USERS_CHILD = "users";
    public final static String DB_AVATAR_CHILD = "avatar";
    public final static String DB_EMAIL_CHILD = "email";
    public final static String DB_ID_CHILD = "id";
    public final static String DB_MOBILE_NO_CHILD = "mobile_no";
    public final static String DB_NAME_CHILD = "name";
    public final static String DB_AVATAR_CHILD_DEFAULT = "avatar.png";

    public final static String ORDERS_TO_DETAILS = "order_to_details"; // from orders to details or from dialog
    public final static String DETAILS_TO_TIME_LINE = "details_to_time_line";  // from details to time line
    public final static String TIME_LINE_TO_DETAILS = "time_line_to_details";  // from time line to details

    public final static String DB_PRE_ORDERS_CHILD = "pre_orders";
    public final static String DB_PRE_ORDERS_EMP_STATUS_CHILD = "emp_status";
    public final static String DB_ORDERS_CHILD = "orders";
    public final static String DB_ORDERS_USER_STATUS_CHILD = "user_status";
    public final static String DB_ORDERS_DRIVER_STATUS_CHILD = "emp_status";
    public final static String DB_ORDERS_USER_MODEL_ID_CHILD = "userModel/id";
    public final static String DB_ORDERS_DRIVER_MODEL_ID_CHILD = "driverModel/id";

    public final static String DB_STATUS_CHILD = "status";
    public final static String DB_STATUS_CHILD_AVAILABLE = "available"; // employees
    public final static String DB_STATUS_CHILD_UNAVAILABLE = "unavailable"; // employees

    public final static String DB_STATUS_CHILD_ACCEPTED = "accepted"; // employees
    public final static String DB_STATUS_CHILD_REACHED = "reached"; // orders
    public final static String DB_STATUS_CHILD_IN_PROGRESS = "in_progress"; // order
    public final static String DB_STATUS_CHILD_CANCELLED = "cancelled"; // orders
    public final static String DB_STATUS_CHILD_COMPLETED = "completed"; // orders

    public final static String DB_ORDER_ID = "order_id"; // Shared ref
    public final static String DB_ORDER_PENDING = "pending"; // Shared ref
    public final static String DB_ORDER_IN_PROGRESS = "inProgress"; // query, = false or true.

    public final static String DB_EMPLOYEES_CHILD = "employees";
    public final static String DB_EMPLOYEES_LOCATION_CHILD = "location";

    public final static String FETCH_ADDRESS_INTENT_ACTION = "fetch_address";
    public final static String FETCH_ADDRESS_RESULT = "address_result";
    public final static String FETCH_ADDRESS_COUNTRY = "address_country";
    public final static String FETCH_ADDRESS_COUNTRY_SAUDI = "السعودية";

    public final static String STORAGE_UPLOAD_USER_IMAGES_FOLDER_NAME = "users/";
    public final static String STORAGE_UPLOAD_EMPLOYEE_IMAGES_FOLDER_NAME = "employees/";
    public final static String STORAGE_IMAGE_EXTENSION_TYPE = ".jpg";

    public final static int PROFILE_PICK_IMAGE_REQUEST = 71;

    //public final static Context CONTEXT = ControlActivity.getInstance().getApplicationContext();

    public final static int LOCATION_PERMISSION_CODE = 10;





    private static ArrayList<RequestLocation> listPoints = new ArrayList<>();


    public static void addToListPoints(int index, LatLng latLng, String locationName) {
        listPoints.set(index, new RequestLocation(latLng, locationName));
    }

    public static void addToListPoints(LatLng latLng, String locationName) {
        listPoints.add(new RequestLocation(latLng, locationName));
    }

    public static RequestLocation getFromListPoints(int index) {
        return listPoints.get(index);
    }

    public static int getListPointsSize() {
        return listPoints.size();
    }

    public static void clearListPoints() {
        if(listPoints.size() > 0)
            listPoints.clear();
    }





    public static String getRequestURL(LatLng origin, LatLng wayPoint, LatLng destination) {
        // origin to wayPoint, wayPoint to destination (origin => destination throw wayPoint)

        // Value of origin (origin = current)
        String str_org = "origin=" + origin.latitude + "," + origin.longitude;
        // Value of destination
        String str_destination = "destination=" + destination.latitude + "," + destination.longitude;
        // Set value enable the sensor
        String sensor = "sensor=false";
        // Mode for find direction
        String mode = "mode-driving";
        // value of Way point
        String way_point = "waypoints=" + wayPoint.latitude + "," + wayPoint.longitude;

        // Build the full param
        String param = str_org + "&" + str_destination + "&" + way_point + "&" + sensor + "&" + mode;
        // Output format
        String output = "json";

        // Create url to request
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
    }


    public static String getRequestURL(LatLng origin, LatLng destination) {
        // Value of origin (origin = current)
        String str_org = "origin=" + origin.latitude + "," + origin.longitude;
        // Value of destination
        String str_destination = "destination=" + destination.latitude + "," + destination.longitude;
        // Set value enable the sensor
        String sensor = "sensor=false";
        // Mode for find direction
        String mode = "mode-driving";
        // value of Way point
        // Build the full param
        String param = str_org + "&" + str_destination + "&" + sensor + "&" + mode;
        // Output format
        String output = "json";

        // Create url to request
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
    }


    public static String getTimeDate(long timeStamp) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timeStamp * 1000L);
        return DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
    }



    private Context mContext;

    public GeneralData(Context context) {
        this.mContext = context;
    }


    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;

        try {
            if(cm != null)
                netInfo = cm.getActiveNetworkInfo();
        }catch (NullPointerException ex) {
            ExceptionHandling.exceptionOccurred("AppConstants", "NullPointerException" + ex.getMessage());
        }

        return (netInfo != null) && netInfo.isConnectedOrConnecting();
    }





    public static Bitmap getBitmapFromURL(String Url) {
        try {
            URL url = new URL(Url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;
        } catch (MalformedURLException e) {
            ExceptionHandling.exceptionOccurred("GeneralData", "MalformedURLException " + e.getMessage());
        } catch (IOException e) {
            ExceptionHandling.exceptionOccurred("GeneralData", "IOException " + e.getMessage());
        }

        return null;
    }

}
