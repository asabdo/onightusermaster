package com.android.wanetusermaster.models;

/**
 * Created by Samer AlShurafa on 2/11/2018.
 */

public interface LoadFragmentListener {

    void loadFragment(String fragmentName);

}
