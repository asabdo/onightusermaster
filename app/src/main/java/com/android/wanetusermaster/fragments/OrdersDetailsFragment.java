package com.android.wanetusermaster.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.wanetusermaster.R;
import com.android.wanetusermaster.models.ExceptionHandling;
import com.android.wanetusermaster.models.GeneralData;
import com.android.wanetusermaster.models.LoadFragmentListener;
import com.android.wanetusermaster.models.Orders;
import com.android.wanetusermaster.models.OrdersListener;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;


public class OrdersDetailsFragment extends Fragment {

    private LoadFragmentListener loadFragmentListener;
    private OrdersListener ordersListener;
    private Orders orders = null;


    public OrdersDetailsFragment() { }

    public static OrdersDetailsFragment newInstance() {
        return new OrdersDetailsFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof LoadFragmentListener) {
            loadFragmentListener = (LoadFragmentListener) context;
            ordersListener = (OrdersListener) context;
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_orders_details, container, false);
    }


    public void setOrders(Orders orders) {
        this.orders = orders;
        updateUI();
    }





    private TextView sourceLocationName, destinationLocationName, driverOrderName, orderDate, carOrderModel, carOrderNumber;
    private CircularImageView driverOrderImage;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sourceLocationName = view.findViewById(R.id.sourceLocationName);
        destinationLocationName = view.findViewById(R.id.destinationLocationName);
        orderDate = view.findViewById(R.id.orderDate);
        driverOrderName = view.findViewById(R.id.driverOrderName);
        carOrderModel = view.findViewById(R.id.carOrderModel);
        carOrderNumber = view.findViewById(R.id.carOrderNumber);

        driverOrderImage = view.findViewById(R.id.driverOrderImage);
        ImageView callImage = view.findViewById(R.id.callImage);

        Button returnToOrdersList = view.findViewById(R.id.return_to_orders_list);
        Button goTimeLineFragment = view.findViewById(R.id.goTimeLineFragment);

        returnToOrdersList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragmentListener.loadFragment("OrdersFragment");
            }
        });


        goTimeLineFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ordersListener.onOrderSelected(orders, GeneralData.DETAILS_TO_TIME_LINE);
            }
        });


        callImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open telephone application and contact driver ..

                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + orders.getDriverModel().getMobile_no()));
                    startActivity(intent);
                } catch (Exception ex) {
                    ExceptionHandling.exceptionOccurred("OrdersDetailsFragment", "Exception: " + ex.getMessage());
                }
            }
        });

    }


    private void updateUI() {

        if(orders != null) {

            sourceLocationName.setText(orders.getSourceLocation().getName());
            destinationLocationName.setText(orders.getDestinationLocation().getName());
            orderDate.setText(GeneralData.getTimeDate(orders.getService_time()));
            driverOrderName.setText(orders.getDriverModel().getName());
            carOrderModel.setText(orders.getDriverModel().getCar_model());
            carOrderNumber.setText(orders.getDriverModel().getCar_no());

            // Driver image :
            if(!orders.getDriverModel().getAvatar().isEmpty() &&
                    !orders.getDriverModel().getAvatar().equalsIgnoreCase(GeneralData.DB_AVATAR_CHILD_DEFAULT)) {

                Picasso.with(getContext())
                        .load(orders.getDriverModel().getAvatar())
                        .placeholder(R.drawable.profile_user)
                        .error(R.drawable.logo_orange)
                        .into(driverOrderImage);

            }

        } // end onCreateView


    } // end updateUI

}



