package com.android.wanetusermaster.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wanetusermaster.R;
import com.android.wanetusermaster.models.ExceptionHandling;
import com.android.wanetusermaster.models.GeneralData;
import com.android.wanetusermaster.models.User;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;


public class ProfileFragment extends Fragment {

    private Uri uriFilePath = null;

    private DatabaseReference databaseReference = null;
    private StorageReference storageReference = null;

    private EditText profileNameEdit, profileEmailEdit;
    private CircularImageView profileImageEdit;

    private SharedPreferences sharedPreferences = null;
    private SharedPreferences.Editor editor = null;


    public ProfileFragment() {}

    /*
    public static ProfileFragment newInstance(Users user) {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(GeneralData.EXTRA_USER_OBJECT, user);
        profileFragment.setArguments(bundle);

        return profileFragment;
    }
    */

    public static ProfileFragment newInstance() {
        //ProfileFragment profileFragment = new ProfileFragment();
        //Bundle bundle = new Bundle();
        //bundle.putParcelable(GeneralData.EXTRA_USER_OBJECT, user);
        //profileFragment.setArguments(bundle);

        return new ProfileFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }





    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        profileNameEdit = view.findViewById(R.id.profileNameEdit);
        profileEmailEdit = view.findViewById(R.id.profileEmailEdit);
        EditText profilePhoneNoEdit = view.findViewById(R.id.profilePhoneNoEdit);
        profileImageEdit = view.findViewById(R.id.profileImageEdit);
        Button profileEditBtn = view.findViewById(R.id.profileEditBtn);


        if(getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences(GeneralData.FILE_NAME, MODE_PRIVATE);
        }

        if(sharedPreferences != null) {
            editor = sharedPreferences.edit();
        }


        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if(currentUser != null) {
            profilePhoneNoEdit.setText(currentUser.getPhoneNumber());

            // For update user info
            databaseReference = FirebaseDatabase.getInstance().getReference(GeneralData.DB_USERS_CHILD)
                    .child(currentUser.getUid());

            // For upload the image
            storageReference = FirebaseStorage.getInstance().getReference().child(
                    GeneralData.STORAGE_UPLOAD_USER_IMAGES_FOLDER_NAME
                            + currentUser.getUid()
                            + GeneralData.STORAGE_IMAGE_EXTENSION_TYPE);
        }



        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                if (user != null) {
                    if (user.getName() != null && !user.getName().isEmpty()) {
                        profileNameEdit.setText(user.getName());
                        profileNameEdit.setTextColor(view.getResources().getColor(R.color.color_main_orange));
                    } else {
                        profileNameEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                        profileNameEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                    }

                    if (user.getEmail() != null && !user.getEmail().isEmpty()) {
                        profileEmailEdit.setText(user.getEmail());
                        profileEmailEdit.setTextColor(view.getResources().getColor(R.color.color_main_orange));
                    } else {
                        profileEmailEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                        profileEmailEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                    }

                    // change profile image
                    if(user.getAvatar() != null && !user.getAvatar().equalsIgnoreCase(GeneralData.DB_AVATAR_CHILD_DEFAULT)) {

                        Picasso.with(getContext())
                                .load(user.getAvatar())
                                .placeholder(R.drawable.profile_user)
                                .error(R.drawable.logo_orange)
                                .into(profileImageEdit);
                    }


                    if(user.getName() != null && user.getEmail() != null && user.getAvatar() != null && sharedPreferences != null) {
                        // If remove the app and reinstall it, check the shared ..
                        String name = sharedPreferences.getString(GeneralData.DB_NAME_CHILD, GeneralData.DEFAULT_VALUE);
                        String email = sharedPreferences.getString(GeneralData.DB_EMAIL_CHILD, GeneralData.DEFAULT_VALUE);
                        String avatar = sharedPreferences.getString(GeneralData.DB_AVATAR_CHILD,
                                GeneralData.DB_AVATAR_CHILD_DEFAULT);

                        if(name.equalsIgnoreCase(GeneralData.DEFAULT_VALUE))
                            editor.putString(GeneralData.DB_NAME_CHILD, user.getName());
                        if(email.equalsIgnoreCase(GeneralData.DEFAULT_VALUE))
                            editor.putString(GeneralData.DB_EMAIL_CHILD, user.getEmail());
                        if(avatar.equalsIgnoreCase(GeneralData.DB_AVATAR_CHILD_DEFAULT))
                            editor.putString(GeneralData.DB_AVATAR_CHILD, user.getAvatar());

                        editor.apply();
                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { }

        });




        final GeneralData generalData = new GeneralData(getContext());

        if(!generalData.isOnline()) { // check user information within offline ..

            if(sharedPreferences != null) {

                String name = sharedPreferences.getString(GeneralData.DB_NAME_CHILD, GeneralData.DEFAULT_VALUE);
                String email = sharedPreferences.getString(GeneralData.DB_EMAIL_CHILD, GeneralData.DEFAULT_VALUE);
                String avatar = sharedPreferences.getString(GeneralData.DB_AVATAR_CHILD, GeneralData.DB_AVATAR_CHILD_DEFAULT);

                if (!name.equalsIgnoreCase(GeneralData.DEFAULT_VALUE)) {
                    profileNameEdit.setText(name);
                    profileNameEdit.setTextColor(view.getResources().getColor(R.color.color_main_orange));
                } else {
                    profileNameEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                    profileNameEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                }

                if (!email.equalsIgnoreCase(GeneralData.DEFAULT_VALUE)) {
                    profileEmailEdit.setText(email);
                    profileEmailEdit.setTextColor(view.getResources().getColor(R.color.color_main_orange));
                } else {
                    profileEmailEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                    profileEmailEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                }

                if (!avatar.equalsIgnoreCase(GeneralData.DB_AVATAR_CHILD_DEFAULT)) {

                    Picasso.with(getContext())
                            .load(avatar)
                            .placeholder(R.drawable.profile_user)
                            .error(R.drawable.logo_orange)
                            .into(profileImageEdit);

                }

            } // if not online

        }



        // Open the gallery ..
        profileImageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });


        profileEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // first step is check the internet connection ..
                if(!generalData.isOnline()) {
                    showAlertDialog(getResources().getString(R.string.no_internet_connection),
                            getResources().getString(R.string.no_internet_connection_description));
                } else if(profileNameEdit.getText().toString().trim().isEmpty()) {
                    profileNameEdit.setError(view.getResources().getString(R.string.error_enter_user_name));
                } else if(profileEmailEdit.getText().toString().trim().isEmpty()) {
                    profileEmailEdit.setError(view.getResources().getString(R.string.error_enter_user_email));
                } else if(!ifEmailCorrect(profileEmailEdit.getText().toString())) { // not correct ..
                    profileEmailEdit.setError(view.getResources().getString(R.string.error_email_in_correct));
                } else {
                    // Update the Firebase database .. name, email
                    String name = profileNameEdit.getText().toString().trim();
                    String email = profileEmailEdit.getText().toString().trim();

                    databaseReference.child(GeneralData.DB_NAME_CHILD).setValue(name);
                    databaseReference.child(GeneralData.DB_EMAIL_CHILD).setValue(email);

                    //boolean update_result = the_user.updateUserProfile(name, email);

                    //if(update_result){

                    if(sharedPreferences != null) {
                        editor.putString(GeneralData.DB_NAME_CHILD, name);
                        editor.putString(GeneralData.DB_EMAIL_CHILD, email);
                        editor.apply();
                    }

                    if(uriFilePath != null) {
                        uploadImage();
                    }
                    else {
                        Toast.makeText(getContext(), getResources().getString(R.string.profile_success_update_info),
                                Toast.LENGTH_SHORT).show();
                    }
                    //} else {
                    //    Toast.makeText(getContext(), getResources().getString(R.string.error_edit_info), Toast.LENGTH_SHORT).show();
                    //}
                }
            }
        });

    } // end onViewCreated






    private boolean ifEmailCorrect(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        this.startActivityForResult(Intent.createChooser(intent, "Ø§Ø®ØªØ± ØµÙˆØ±Ø©"), GeneralData.PROFILE_PICK_IMAGE_REQUEST);
    }





    private void showAlertDialog(String text1, String text2) {
        final AlertDialog.Builder progressDialog = new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);

        TextView title = new TextView(getContext());
        title.setText(getResources().getString(R.string.request_service));
        title.setTextSize(20);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_main_orange));

        TextView messageText = new TextView(getContext());
        messageText.setText(Html.fromHtml("<br><br>" + text1 + "<br>" + text2 + "<br>" ));
        messageText.setTextSize(15);
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        messageText.setTextColor(getResources().getColor(R.color.color_magor_orange));

        progressDialog.setCustomTitle(title);
        progressDialog.setView(messageText);
        //progressDialog.setCancelable(false);
        progressDialog.setPositiveButton("Ø­Ø³Ù†Ø§",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                dialog.cancel();
            }
        });

        progressDialog.show();
    }





    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Hide the keyboard if it is show before go to another fragment

        final InputMethodManager imm;
        try {
            if (getActivity() != null) {
                imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null && getView() != null) {
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                }
            }
        }catch (NullPointerException ex) {
            ExceptionHandling.exceptionOccurred("ProfileFragment", "NullPointerException: " + ex.getMessage());
        }
    }






    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == GeneralData.PROFILE_PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                intent != null && intent.getData() != null) {

            uriFilePath = intent.getData();

            try {
                Bitmap bitmap = null;
                if(getActivity() != null) {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uriFilePath);
                }
                if(bitmap != null) {
                    profileImageEdit.setImageBitmap(bitmap);
                }

            } catch (IOException e) {
                ExceptionHandling.exceptionOccurred("ProfileFragment", "IOException: " + e.getMessage());
            }

        }
    }





    private void uploadImage() {

        if (uriFilePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle(getResources().getString(R.string.profile_edit_info));
            progressDialog.show();

            // Reduce the size of image ..

            Bitmap bitmap = null;

            try {
                if(getActivity() != null)
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uriFilePath);
            } catch (IOException e) {
                ExceptionHandling.exceptionOccurred("ProfileFragment", "IOException: " + e.getMessage());
            }

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if(bitmap != null)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

            //final BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 2;
            // Decode bitmap with inSampleSize set
            //options.inJustDecodeBounds = false;

            //Bitmap newBitmap = null;

            //if(bitmap != null)
            //  newBitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, true);


            //byte[] imageBytes = byteArrayOutputStream.toByteArray();
            //String newPath = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap,
            //        "Title", null);
            //Uri newFile;
            UploadTask uploadTask;

            //newFile = Uri.fromFile(f);

            //if(newPath != null) {
            //Toast.makeText(getContext(), "Real Path: " + realPath, Toast.LENGTH_LONG).show();
            //newFile = Uri.parse(newPath);
            //uploadTask = storageReference.putFile(newFile);
            //} else {
            //Toast.makeText(getContext(), "New Path null", Toast.LENGTH_LONG).show();

            //}


            uploadTask = storageReference.putFile(uriFilePath);

            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    progressDialog.setMessage("قيد التنفيذ " + (int) progress + "%");
                }
            }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    ExceptionHandling.exceptionOccurred("ProfileFragment", "Exception: " + exception.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {

                            String downloadUrlPath = "";

                            if(uri != null) {
                                downloadUrlPath = uri.toString();
                            }

                            if(sharedPreferences != null && !downloadUrlPath.isEmpty()) {
                                editor.putString(GeneralData.DB_AVATAR_CHILD, downloadUrlPath);
                                editor.apply();
                            }

                            if(!downloadUrlPath.isEmpty())
                                databaseReference.child(GeneralData.DB_AVATAR_CHILD).setValue(downloadUrlPath);

                            if(progressDialog.isShowing()) {
                                progressDialog.hide();
                                progressDialog.dismiss();
                            }

                            Toast.makeText(getContext(), getResources().getString(R.string.profile_success_update_info),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });


        } // end if file path != null

    } // end uploadImage






}





