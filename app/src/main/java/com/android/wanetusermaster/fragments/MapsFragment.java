package com.android.wanetusermaster.fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.android.wanetusermaster.R;
import com.android.wanetusermaster.models.DirectionsParser;
import com.android.wanetusermaster.models.Employee;
import com.android.wanetusermaster.models.EmployeeLocation;
import com.android.wanetusermaster.models.ExceptionHandling;
import com.android.wanetusermaster.models.FetchAddressIntentService;
import com.android.wanetusermaster.models.GeneralData;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wanetusermaster.models.LoadFragmentListener;
import com.android.wanetusermaster.models.OrderLocation;
import com.android.wanetusermaster.models.Orders;
import com.android.wanetusermaster.models.OrdersListener;
import com.android.wanetusermaster.models.PreOrders;
import com.android.wanetusermaster.models.RequestLocation;
import com.android.wanetusermaster.models.SoundService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.ui.IconGenerator;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.Context.MODE_PRIVATE;


public class MapsFragment extends Fragment implements OnMapReadyCallback {


    private MapView mMapView;
    private ImageView destinationFakeMarker;
    private Button chooseDestinationBtn;
    private LinearLayout servicesLayout, locationInformationLayout ;
    private TextView sourceLocationName, destinationLocationName;

    private GoogleMap mMap;
    private MarkerOptions markerOptions;
    private Location currentLocation = null;


    private DatabaseReference databaseEmployeesReference = null;
    private ValueEventListener valueEventListener1 = null, valueEventListener2 = null;
    private ChildEventListener childEventListener1 = null;
    private DatabaseReference databaseReference = null;

    private ArrayList<Employee> employeesList = new ArrayList<>();

    private String ifTherePendingOrder = "no";
    private String orderId = "";

    private String userId = "";

    private ProgressDialog orderDialog = null;
    private Dialog closeDialog = null;
    private Dialog driverDialog = null;

    private boolean isOrderAccepted = false;
    private boolean isOrderCompleted = false;


    private OrdersListener ordersListener;

    private String url = "";
    private String responseString = "";
    private SharedPreferences sharedPreferences = null;

    private Orders pendingOrder = null;
    private Employee orderEmployee = null;

    private final String[] changeLocation = {"destination"};

    private String locationName;
    private String countryName;

    private Boolean[] ifSaudi = {true, false}; // first: source, second: destination, true: saudi, false: not (problem)


    private boolean isRemovedFromUserSide = false, isRemovedFromCloudSide = false;

    private SharedPreferences.Editor editor = null;


    public MapsFragment() { }

    public static MapsFragment newInstance() {
        return new MapsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof LoadFragmentListener) {
            this.ordersListener = (OrdersListener) context;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }






    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mMapView = view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately


        destinationFakeMarker = view.findViewById(R.id.destinationFakeMarker);
        chooseDestinationBtn = view.findViewById(R.id.chooseDestinationBtn);
        final Button requestNowtBtn = view.findViewById(R.id.requestNowtBtn);
        servicesLayout = view.findViewById(R.id.servicesLayout);
        locationInformationLayout = view.findViewById(R.id.locationInformationLayout);
        sourceLocationName = view.findViewById(R.id.sourceLocationName);
        destinationLocationName = view.findViewById(R.id.destinationLocationName);
        RadioRealButtonGroup servicesGroup = view.findViewById(R.id.servicesGroup);


        try {
            if (getActivity() != null)
                MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            ExceptionHandling.exceptionOccurred("MapsFragment", "Exception: " + e.getMessage());
        }

        mMapView.getMapAsync(this);


        databaseEmployeesReference = FirebaseDatabase.getInstance().getReference(GeneralData.DB_EMPLOYEES_CHILD);
        databaseReference = FirebaseDatabase.getInstance().getReference();


        if (getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences(GeneralData.FILE_NAME, MODE_PRIVATE);
        }

        if (sharedPreferences != null) {
            editor = sharedPreferences.edit();
        }


        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null)
            userId = currentUser.getUid();


        GeneralData generalData = new GeneralData(getContext());

        if (generalData.isOnline()) {

            // default values ..
            ifTherePendingOrder = "no";
            activateFragmentComponents();
            isOrderAccepted = false;

            // query to check if there is order not completed related to this customer ,,
            // update orderId and ifTherePendingOrder if there is pending order ,,
            // get all pending orders and check if there is order to this user (max: drivers: 15 => not completed => 15) Best :)
            // get all orders to this user and check if there is pending order to him\her (user: 3 orders, 100 orders ..)

            databaseReference.child(GeneralData.DB_ORDERS_CHILD)
                    .orderByChild(GeneralData.DB_ORDER_IN_PROGRESS).equalTo(true)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Orders orders;

                            for (DataSnapshot child : dataSnapshot.getChildren()) {
                                orders = child.getValue(Orders.class);

                                if (orders != null && null != orders.getUserModel()) {
                                    if (userId.equals(orders.getUserModel().getId())) {
                                        ifTherePendingOrder = "yes";
                                        orderId = orders.getId();
                                        pendingOrder = orders;
                                        isOrderAccepted = true;

                                        if (editor != null) {
                                            editor.putString(GeneralData.DB_ORDER_ID, orderId);
                                            editor.putString(GeneralData.DB_ORDER_PENDING, "yes");
                                            editor.apply();
                                        }

                                        deActivateFragmentComponents();

                                        if (pendingOrder != null)
                                            showDriverInformationDialog(getContext(), pendingOrder.getDriverModel());

                                        break;
                                    }
                                }
                            } // end for loop
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) { }

                    });

        } else if (sharedPreferences != null) {
            ifTherePendingOrder = sharedPreferences.getString(GeneralData.DB_ORDER_PENDING, GeneralData.DEFAULT_VALUE);
            orderId = sharedPreferences.getString(GeneralData.DB_ORDER_ID, GeneralData.DEFAULT_VALUE);

            if (ifTherePendingOrder.equalsIgnoreCase("yes")) {
                deActivateFragmentComponents();
                isOrderAccepted = true;
            } else {
                ifTherePendingOrder = "no";
                activateFragmentComponents();
                isOrderAccepted = false;
                isOrderCompleted = false;
            }

        }






        chooseDestinationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (employeesList.size() > 0) {

                    if (mMap != null && markerOptions != null) {

                        if (GeneralData.getListPointsSize() == 2) {

                            if (ifSaudi[0] && ifSaudi[1]) {
                                destinationFakeMarker.setVisibility(View.INVISIBLE);
                                chooseDestinationBtn.setVisibility(View.INVISIBLE);

                                servicesLayout.setVisibility(View.VISIBLE);

                                RequestLocation sourceLocation = GeneralData.getFromListPoints(0);
                                RequestLocation destinationLocation = GeneralData.getFromListPoints(1);

                                // Add the marker to the map
                                IconGenerator iconGenerator = new IconGenerator(getContext());

                                iconGenerator.setColor(Color.GREEN);

                                mMap.addMarker(markerOptions
                                        .position(sourceLocation.getLatLng())
                                        .icon(BitmapDescriptorFactory.fromBitmap(
                                                iconGenerator.makeIcon(getString(R.string.transfer_from))))
                                        .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


                                iconGenerator.setColor(Color.RED);
                                mMap.addMarker(markerOptions
                                        .position(destinationLocation.getLatLng())
                                        .icon(BitmapDescriptorFactory.fromBitmap(
                                                iconGenerator.makeIcon(getString(R.string.transfer_to))))
                                        .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


                                // Draw the direction line between tow markers ..
                                url = GeneralData.getRequestURL(sourceLocation.getLatLng(), destinationLocation.getLatLng());

                                TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
                                taskRequestDirections.execute(url);

                            } else {
                                showAlertDialog(getResources().getString(R.string.incorrect_choices),
                                        getResources().getString(R.string.incorrect_choices_description));
                            }

                        } else {
                            ExceptionHandling.exceptionOccurred("MapsFragment", "List points size not equal 2");
                        }

                    } // end if Map != null
                } // end if Employees List size > 0
                else {
                    showAlertDialog(getResources().getString(R.string.no_available_drivers),
                            getResources().getString(R.string.no_available_drivers_try_again));
                }
            }
        });

        final int[] serviceSelected = {0};

        servicesGroup.setOnClickedButtonListener(new RadioRealButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(RadioRealButton button, int position) {
                serviceSelected[0] = position;
            }
        });


        requestNowtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Check if the internet connection or no ..
                GeneralData generalData = new GeneralData(getContext());
                if (generalData.isOnline()) {

                    if (employeesList.size() > 0) {
                        String serviceSelectedType = "motorcycle_service";

                        switch (serviceSelected[0]) {
                            case 0:
                                serviceSelectedType = "motorcycle_service";
                                break;
                            case 1:
                                serviceSelectedType = "car_service";
                                break;
                            case 2:
                                serviceSelectedType = "truck_service";
                                break;
                            case 3:
                                serviceSelectedType = "oneight_service";
                                break;
                        } // end switch

                        boolean ifAvailable = false;

                        for (int i = 0; i < employeesList.size(); i++) {
                            if (employeesList.get(i).getServiceType().equalsIgnoreCase(serviceSelectedType)) {
                                ifAvailable = true;
                                break;
                            }
                        }

                        if (ifAvailable) { // there is available service as the user wanted, from list

                            RequestLocation sourceLocation = GeneralData.getFromListPoints(0);
                            LatLng sourceLatLng = sourceLocation.getLatLng();

                            RequestLocation destinationLocation = GeneralData.getFromListPoints(1);
                            LatLng destinationLatLng = destinationLocation.getLatLng();

                            if (sourceLatLng != null) {
                                if (destinationLatLng != null) {

                                    orderId = databaseReference.child(GeneralData.DB_ORDERS_CHILD).push().getKey();

                                    PreOrders preOrders = new PreOrders(orderId, serviceSelectedType,
                                            new OrderLocation(sourceLatLng.latitude, sourceLatLng.longitude, sourceLocation.getLocationName()),
                                            new OrderLocation(destinationLatLng.latitude, destinationLatLng.longitude, destinationLocation.getLocationName()));

                                    databaseReference.child(GeneralData.DB_PRE_ORDERS_CHILD).child(orderId).setValue(preOrders);

                                    isRemovedFromCloudSide = false;

                                    showOrderDialog();

                                } else {
                                    ExceptionHandling.exceptionOccurred("MapsFragment", "Destination LatLng : null");
                                }
                            } else {
                                ExceptionHandling.exceptionOccurred("MapsFragment", "Source LatLng : null");
                            }

                        } else { // no services available from list ..
                            showAlertDialog(getResources().getString(R.string.no_service_available_drivers),
                                    getResources().getString(R.string.no_available_drivers_try_again));
                        }

                    }

                } else { // no internet ..
                    showAlertDialog(getResources().getString(R.string.no_internet_connection),
                            getResources().getString(R.string.no_internet_connection_description));
                }
            }
        });


        if (getContext() != null &&
                ActivityCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                    GeneralData.LOCATION_PERMISSION_CODE);
        }


        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                if (location != null) {
                    currentLocation = location;
                    final LatLng latLng; // = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

                    // ToDo remove this ,, for test
                    latLng = new LatLng(24.6417714, 46.7249202);

                    if (currentLocation != null) {
                        //latLng = new LatLng(currentLocation.getLongitude(), currentLocation.getLatitude());

                        Intent serviceIntent = new Intent(getContext(), FetchAddressIntentService.class);
                        serviceIntent.putExtra("Latitude", latLng.latitude);
                        serviceIntent.putExtra("Longitude", latLng.longitude);
                        getContext().startService(serviceIntent);

                        getActivity().registerReceiver(new BroadcastReceiver() {
                            @Override
                            public void onReceive(Context context, Intent intent) {
                                locationName = intent.getStringExtra(GeneralData.FETCH_ADDRESS_RESULT);
                                countryName = intent.getStringExtra(GeneralData.FETCH_ADDRESS_COUNTRY);

                                if (GeneralData.getListPointsSize() == 0) { // first time only ..
                                    GeneralData.addToListPoints(latLng, locationName);
                                    if (!countryName.equalsIgnoreCase(GeneralData.FETCH_ADDRESS_COUNTRY_SAUDI)) {
                                        ifSaudi[0] = false; // source
                                    }
                                }

                            }
                        }, new IntentFilter(GeneralData.FETCH_ADDRESS_INTENT_ACTION));

                    }

                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
                    mMap.animateCamera(cameraUpdate);

                }
            }
        });


        sourceLocationName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chooseDestinationBtn.getVisibility() != View.VISIBLE)
                    return;

                if (!changeLocation[0].equalsIgnoreCase("source")) { // == destination
                    // Get the destination location and save it ..
                    if (mMap != null) {
                        LatLng latLngTarget = mMap.getCameraPosition().target;

                        if (countryName.equalsIgnoreCase(GeneralData.FETCH_ADDRESS_COUNTRY_SAUDI)) {
                            if (GeneralData.getListPointsSize() == 1)
                                GeneralData.addToListPoints(latLngTarget, locationName);
                            else if (GeneralData.getListPointsSize() == 2) // avoid size = 0 : Exception
                                GeneralData.addToListPoints(1, latLngTarget, locationName);

                            ifSaudi[1] = true;
                        } else {
                            ifSaudi[1] = false; // destination not Saudi
                        }

                    } // end map != null

                    changeLocation[0] = "source";
                    sourceLocationName.setBackgroundColor(getResources().getColor(R.color.location_layout_selected));
                    destinationLocationName.setBackgroundColor(getResources().getColor(R.color.location_layout_not_selected));
                }

            }
        });

        destinationLocationName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chooseDestinationBtn.getVisibility() != View.VISIBLE)
                    return;

                if (!changeLocation[0].equalsIgnoreCase("destination")) { // == source
                    // Get the source location and save it ..
                    if (mMap != null) {
                        LatLng latLngTarget = mMap.getCameraPosition().target;

                        if (countryName.equalsIgnoreCase(GeneralData.FETCH_ADDRESS_COUNTRY_SAUDI)) {
                            if (GeneralData.getListPointsSize() == 0)
                                GeneralData.addToListPoints(latLngTarget, locationName);
                            else if (GeneralData.getListPointsSize() == 1) // mostly ..
                                GeneralData.addToListPoints(0, latLngTarget, locationName);

                            ifSaudi[0] = true;
                        } else {
                            ifSaudi[0] = false; // source not Saudi
                        }

                    } // end map != null

                    changeLocation[0] = "destination";
                    sourceLocationName.setBackgroundColor(getResources().getColor(R.color.location_layout_not_selected));
                    destinationLocationName.setBackgroundColor(getResources().getColor(R.color.location_layout_selected));
                }
            }
        });


    } // end onViewCreated ..






    private void disableMapContent() {
        if (mMap != null) {
            mMap.getUiSettings().setScrollGesturesEnabled(false);
            mMap.getUiSettings().setZoomGesturesEnabled(false);
            mMap.getUiSettings().setZoomControlsEnabled(false);
        }
    }

    private void deActivateFragmentComponents() {
        servicesLayout.setVisibility(View.INVISIBLE);
        chooseDestinationBtn.setVisibility(View.INVISIBLE);
        destinationFakeMarker.setVisibility(View.INVISIBLE);
        locationInformationLayout.setVisibility(View.INVISIBLE);
        disableMapContent();
    }


    private void enableMapContent() {
        if (mMap != null) {
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
        }
    }

    private void activateFragmentComponents() {
        chooseDestinationBtn.setVisibility(View.VISIBLE);
        destinationFakeMarker.setVisibility(View.VISIBLE);
        locationInformationLayout.setVisibility(View.VISIBLE);
        changeLocation[0] = "destination";
        sourceLocationName.setText(getResources().getString(R.string.your_current_location));
        sourceLocationName.setBackgroundColor(getResources().getColor(R.color.location_layout_not_selected));
        destinationLocationName.setText(getResources().getString(R.string.your_destination_location));
        destinationLocationName.setBackgroundColor(getResources().getColor(R.color.location_layout_selected));
        addAllMarkerToMap();
    }




    @Override
    public void onMapReady(GoogleMap googleMap) {
        if(mMap == null)
            mMap = googleMap;

        mMap.clear();
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        if(ifTherePendingOrder.equalsIgnoreCase("yes")) {
            disableMapContent();
        } else {
            enableMapContent();
        }


        if (getContext() != null && getActivity() != null && ActivityCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                    GeneralData.LOCATION_PERMISSION_CODE);
        } else if(mMap != null) {
            mMap.setMyLocationEnabled(true);
        }

        if (markerOptions == null) {
            markerOptions = new MarkerOptions();
        }


        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (getContext() == null || getActivity() == null)
                    return;

                if(chooseDestinationBtn.getVisibility() == View.INVISIBLE)
                    return;


                LatLng[] lastTarget = new LatLng[2];
                Intent serviceIntent = new Intent(getContext(), FetchAddressIntentService.class);

                lastTarget[0] = mMap.getCameraPosition().target; // new target

                if (lastTarget[0] != lastTarget[1] /*old target*/) {
                    lastTarget[1] = lastTarget[0];

                    // get address ..
                    serviceIntent.putExtra("Latitude", lastTarget[0].latitude);
                    serviceIntent.putExtra("Longitude", lastTarget[0].longitude);
                    getContext().startService(serviceIntent);

                    AddressReceiver myReceiver = new AddressReceiver();
                    IntentFilter intentFilter = new IntentFilter(GeneralData.FETCH_ADDRESS_INTENT_ACTION);
                    getActivity().registerReceiver(myReceiver, intentFilter);

                }
            }
        });


        addAllMarkerToMap();
    } // end onMapReady






    @Override
    public void onStart() {
        super.onStart();


        valueEventListener1 = databaseEmployeesReference
                .orderByChild(GeneralData.DB_STATUS_CHILD).equalTo(GeneralData.DB_STATUS_CHILD_AVAILABLE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        employeesList.clear(); // reset the list

                        Employee employee;

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            employee = postSnapshot.getValue(Employee.class);
                            employeesList.add(employee);
                        }

                        // Apply new changes only if no pending order ..
                        if (sharedPreferences != null) {
                            if(sharedPreferences.getString(GeneralData.DB_ORDER_PENDING, "no")
                                    .equalsIgnoreCase("no")) {
                                addAllMarkerToMap();
                            }
                        }


                    } // end onDataChange


                    @Override
                    public void onCancelled(DatabaseError databaseError) { }

                }); // end databaseEmployeesReference





        // Pre Order listener ..
        //if(!preOrderId.isEmpty())
        childEventListener1 = databaseReference.child(GeneralData.DB_PRE_ORDERS_CHILD).child(orderId)
                .addChildEventListener(new ChildEventListener() {

                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) { }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                        PreOrders preOrders = null;

                        if(null != dataSnapshot.getValue(PreOrders.class))
                            preOrders = dataSnapshot.getValue(PreOrders.class);

                        if (preOrders != null) {

                            String status = preOrders.getStatus();

                            if (status != null && status.equalsIgnoreCase(GeneralData.DB_STATUS_CHILD_ACCEPTED)) {
                                if (orderDialog != null && orderDialog.isShowing()) {
                                    orderDialog.hide();
                                    orderDialog.dismiss();
                                }


                                disableMapContent();

                                locationInformationLayout.setVisibility(View.INVISIBLE);
                                servicesLayout.setVisibility(View.INVISIBLE);


                                if(sharedPreferences != null) {
                                    editor.putString(GeneralData.DB_ORDER_PENDING, "yes");
                                    editor.putString(GeneralData.DB_ORDER_ID, orderId);
                                    editor.apply();
                                }



                                // play notification mp3 file
                                if(getActivity() != null) {
                                    getActivity().startService(new Intent(getActivity(), SoundService.class));
                                }

                                orderEmployee = preOrders.getDriverModel();

                                if(pendingOrder == null) { // if user request order from first time (lunch app)
                                    databaseReference.child(GeneralData.DB_ORDERS_CHILD)
                                            .orderByChild(GeneralData.DB_ID_CHILD).equalTo(orderId)
                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                                                        pendingOrder = child.getValue(Orders.class);
                                                        break;
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {}

                                            });
                                }


                                // show driver dialog only one time ..
                                if (orderEmployee != null && !isOrderAccepted) {
                                    isOrderCompleted = false;
                                    isOrderAccepted = true;
                                    isRemovedFromUserSide = false;
                                    showDriverInformationDialog(getContext(), orderEmployee);
                                }

                            }

                        }

                    } // end onChildChanged

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                        if (orderDialog != null && orderDialog.isShowing()) {
                            orderDialog.hide();
                            orderDialog.dismiss();
                        }

                        Toast.makeText(getContext(), isRemovedFromUserSide + "," + isRemovedFromCloudSide,
                                Toast.LENGTH_LONG).show();

                        if (isRemovedFromUserSide) {
                            isRemovedFromUserSide = false;
                            isRemovedFromCloudSide = true;
                            showCloseOrderDialog(GeneralData.DB_STATUS_CHILD_CANCELLED);
                        }

                        if(!isRemovedFromCloudSide) {
                            isRemovedFromCloudSide = true;
                            isRemovedFromUserSide = false;
                            showAlertDialog(getResources().getString(R.string.no_service_available_drivers),
                                    getResources().getString(R.string.no_available_drivers_try_again));
                        }

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) { }

                    @Override
                    public void onCancelled(DatabaseError databaseError) { }

                }); // end Pre Order listener ..







        // Order listener ..
        valueEventListener2 = databaseReference.child(GeneralData.DB_ORDERS_CHILD).child(orderId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Orders orders = dataSnapshot.getValue(Orders.class);

                        if(orders != null) {

                            String status = orders.getStatus();

                            if(status != null && status.equalsIgnoreCase(GeneralData.DB_STATUS_CHILD_COMPLETED)) {


                                if (sharedPreferences != null) {
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString(GeneralData.DB_ORDER_PENDING, "no");
                                    editor.remove(GeneralData.DB_ORDER_ID);
                                    editor.apply();
                                }
                                ifTherePendingOrder = "no";
                                pendingOrder = null;


                                // clear list points ..
                                GeneralData.clearListPoints();


                                activateFragmentComponents();
                                enableMapContent();


                                // Show dialog about the order is completed only one time ..
                                if(!isOrderCompleted) {
                                    isOrderCompleted = true;
                                    isOrderAccepted = false;

                                    if(driverDialog != null && driverDialog.isShowing()) {
                                        driverDialog.hide();
                                        driverDialog.dismiss();
                                    }

                                    showCloseOrderDialog(GeneralData.DB_STATUS_CHILD_COMPLETED);
                                }

                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) { }

                });



    } // end onStart






    private void addAllMarkerToMap() {
        if(mMap == null)
            return;

        LatLng addLatLng;
        Employee employee;
        EmployeeLocation employeeLocation;

        // clear previous marker if choose destination button is visible ..
        if(chooseDestinationBtn.getVisibility() != View.INVISIBLE)
            mMap.clear();

        if(!ifTherePendingOrder.equalsIgnoreCase("yes")) { // if no pending order
            enableMapContent();
        }


        for (int i = 0; i < employeesList.size(); i++) {
            employee = employeesList.get(i);
            employeeLocation = employee.getLocation();

            addLatLng = new LatLng(employeeLocation.getLatitude(), employeeLocation.getLongitude());

            switch (employee.getServiceType()) {
                case "motorcycle_service":
                    mMap.addMarker(markerOptions.position(addLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.motorcycle_service_icon)));
                    break;
                case "car_service":
                    mMap.addMarker(markerOptions.position(addLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_service_icon)));
                    break;
                case "truck_service":
                    mMap.addMarker(markerOptions.position(addLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_service_icon)));
                    break;
                case "oneight_service":
                    mMap.addMarker(markerOptions.position(addLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.oneight_service_icon)));
                    break;
            } // end switch

        } // end for

    } // end addAllMarkerToMap






    private void showAlertDialog(String text1, String text2) {
        final AlertDialog.Builder progressDialog = new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);

        TextView title = new TextView(getContext());
        title.setText(getResources().getString(R.string.request_service));
        title.setTextSize(20);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_main_orange));

        TextView messageText = new TextView(getContext());
        messageText.setText(Html.fromHtml("<br><br>" + text1 + "<br>" + text2 + "<br>" ));
        messageText.setTextSize(15);
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        messageText.setTextColor(getResources().getColor(R.color.color_magor_orange));

        progressDialog.setCustomTitle(title);
        progressDialog.setView(messageText);
        //progressDialog.setCancelable(false);
        progressDialog.setPositiveButton("Ø­Ø³Ù†Ø§",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                dialog.cancel();
            }
        });

        progressDialog.show();
    }





    private void showOrderDialog() {
        orderDialog = new ProgressDialog(getContext(), R.style.AlertDialogTheme);

        TextView title = new TextView(getContext());
        title.setText(Html.fromHtml("<br>"
                + getResources().getString(R.string.request_service_title)
                + "<br><br>"
                + getResources().getString(R.string.request_service_description)));
        title.setTextSize(17);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_main_orange));

        orderDialog.setCustomTitle(title);
        orderDialog.setCancelable(false);

        // Cancel order dialog
        orderDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.cancel_order_button),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        isRemovedFromUserSide = true;

                        databaseReference.child(GeneralData.DB_PRE_ORDERS_CHILD).child(orderId).setValue(null);

                    }
                });

        orderDialog.show();
    }





    private void showCloseOrderDialog(String status) {
        if(getContext() == null)
            return;

        closeDialog = new Dialog(getContext());

        closeDialog.setContentView(R.layout.dialog_close_order_layout);
        //closeDialog.setCancelable(false);


        Window dialogWindow = closeDialog.getWindow();

        if(closeDialog.getWindow() != null && dialogWindow != null && getActivity() != null) {

            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setGravity(Gravity.START | Gravity.TOP);

            WindowManager manager = getActivity().getWindowManager();
            DisplayMetrics displaymetrics = new DisplayMetrics();
            manager.getDefaultDisplay().getMetrics(displaymetrics);

            layoutParams.x = 75; // The new position of the X coordinates
            layoutParams.y = 300; // The new position of the Y coordinates
            layoutParams.width = displaymetrics.widthPixels - 150; // Width (75 from left, 75 from right)
            layoutParams.alpha = 0.9f; // Transparency

            dialogWindow.setAttributes(layoutParams);
        }


        ImageView statusImage = closeDialog.findViewById(R.id.statusImage);
        TextView statusTitle = closeDialog.findViewById(R.id.statusTitle);
        Button closeDialogButton = closeDialog.findViewById(R.id.closeDialog);

        if(status.equalsIgnoreCase(GeneralData.DB_STATUS_CHILD_COMPLETED)) {
            statusImage.setImageDrawable(getResources().getDrawable(R.drawable.success_message));
            statusTitle.setText(getResources().getString(R.string.order_completed_title));
            statusTitle.setTextColor(getResources().getColor(R.color.color_main_orange));
        } else if(status.equalsIgnoreCase(GeneralData.DB_STATUS_CHILD_CANCELLED)) {
            statusImage.setImageDrawable(getResources().getDrawable(R.drawable.error_message));
            statusTitle.setText(getResources().getString(R.string.cancel_order_from_user));
            statusTitle.setTextColor(getResources().getColor(R.color.color_red));
        }


        closeDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(closeDialog.isShowing()) {
                    closeDialog.hide();
                    closeDialog.dismiss();
                }
            }
        });

        closeDialog.show();
    }







    private void showDriverInformationDialog(Context context, final Employee driver) {

        if(driver == null)
            return;

        driverDialog = new Dialog(context);

        driverDialog.setContentView(R.layout.dialog_driver_information_layout);
        //driverDialog.setCancelable(false);


        Window dialogWindow = driverDialog.getWindow();

        if(driverDialog.getWindow() != null && dialogWindow != null && getActivity() != null) {

            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setGravity(Gravity.START | Gravity.TOP);

            WindowManager manager = getActivity().getWindowManager();
            DisplayMetrics displaymetrics = new DisplayMetrics();
            manager.getDefaultDisplay().getMetrics(displaymetrics);

            layoutParams.x = 50; // The new position of the X coordinates
            layoutParams.y = 250; // The new position of the Y coordinates
            layoutParams.width = displaymetrics.widthPixels - 100; // Width (50 from left, 50 from right)
            layoutParams.height = displaymetrics.heightPixels - 500; // Height (250 from top, 250 from bottom)
            layoutParams.alpha = 0.9f; // Transparency

            dialogWindow.setAttributes(layoutParams);

            driverDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }


        final ImageView driverImage = driverDialog.findViewById(R.id.driverImage);
        final ImageView userImage = driverDialog.findViewById(R.id.userImage);

        TextView driverName = driverDialog.findViewById(R.id.driverName);
        TextView carNumber = driverDialog.findViewById(R.id.carNumber);
        TextView carModel = driverDialog.findViewById(R.id.carModel);
        TextView orderDetails = driverDialog.findViewById(R.id.orderDetails);

        Button contactButton = driverDialog.findViewById(R.id.contactButton);

        driverName.setText(driver.getName());
        carNumber.setText(driver.getCar_no());
        carModel.setText(driver.getCar_model());


        // Drive image
        if(!driver.getAvatar().equalsIgnoreCase(GeneralData.DB_AVATAR_CHILD_DEFAULT) && !driver.getAvatar().isEmpty()) {
            Picasso.with(getContext())
                    .load(driver.getAvatar())
                    .placeholder(R.drawable.profile_user)
                    .error(R.drawable.logo_orange)
                    .into(driverImage);
        }

        String userAvatar = "";

        if(getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences(GeneralData.FILE_NAME, MODE_PRIVATE);
        }

        if(sharedPreferences != null) {
            userAvatar = sharedPreferences.getString(GeneralData.DB_AVATAR_CHILD, GeneralData.DB_AVATAR_CHILD_DEFAULT);
        }



        if(!userAvatar.equalsIgnoreCase(GeneralData.DB_AVATAR_CHILD_DEFAULT)) {
            Picasso.with(getContext())
                    .load(userAvatar)
                    .placeholder(R.drawable.profile_user)
                    .error(R.drawable.logo_orange)
                    .into(userImage);

        }



        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open telephone application and contact driver ..

                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + driver.getMobile_no()));
                    startActivity(intent);
                } catch (Exception ex) {
                    ExceptionHandling.exceptionOccurred("MapsFragment", "Exception: " + ex.getMessage());
                }
            }
        });


        orderDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!orderId.isEmpty() && !orderId.equalsIgnoreCase(GeneralData.DEFAULT_VALUE)) {

                    if(driverDialog.isShowing()) {
                        driverDialog.hide();
                        driverDialog.dismiss();
                    }

                    if(pendingOrder != null)
                        ordersListener.onOrderSelected(pendingOrder, GeneralData.ORDERS_TO_DETAILS);
                }
            }
        });


        driverDialog.show();
    }







    @Override
    public void onResume() { super.onResume(); mMapView.onResume(); }

    @Override
    public void onPause() { super.onPause(); mMapView.onPause(); }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();

        // stop notification mp3 file
        if(getActivity() != null) {
            getActivity().stopService(new Intent(getActivity(), SoundService.class));
        }

        databaseEmployeesReference
                .orderByChild(GeneralData.DB_STATUS_CHILD).equalTo(GeneralData.DB_STATUS_CHILD_AVAILABLE)
                .removeEventListener(valueEventListener1);

        databaseReference.child(GeneralData.DB_ORDERS_CHILD).child(orderId)
                .removeEventListener(valueEventListener2);

        databaseReference.child(GeneralData.DB_PRE_ORDERS_CHILD).child(orderId)
                .removeEventListener(childEventListener1);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onLowMemory() { super.onLowMemory(); mMapView.onLowMemory(); }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case GeneralData.LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (getContext() != null && getActivity() != null && ActivityCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                                GeneralData.LOCATION_PERMISSION_CODE);
                    }
                    mMap.setMyLocationEnabled(true);
                }
                break;
        }
    }







    private class AddressReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {

            if(chooseDestinationBtn.getVisibility() == View.INVISIBLE)
                return;


            locationName = intent.getStringExtra(GeneralData.FETCH_ADDRESS_RESULT);
            countryName = intent.getStringExtra(GeneralData.FETCH_ADDRESS_COUNTRY);

            if(locationName != null) {
                if(changeLocation[0].equalsIgnoreCase("source")) {

                    if(!locationName.isEmpty())
                        sourceLocationName.setText(locationName);

                    LatLng latLngTarget = mMap.getCameraPosition().target;

                    if (countryName.equalsIgnoreCase(GeneralData.FETCH_ADDRESS_COUNTRY_SAUDI)) {
                        if (GeneralData.getListPointsSize() == 0)
                            GeneralData.addToListPoints(latLngTarget, locationName);
                        else // mostly ..
                            GeneralData.addToListPoints(0, latLngTarget, locationName);

                        ifSaudi[0] = true;
                    } else {
                        ifSaudi[0] = false; // source not Saudi
                    }

                } else { // change location = destination ..

                    if(!locationName.isEmpty())
                        destinationLocationName.setText(locationName);

                    LatLng latLngTarget = mMap.getCameraPosition().target;

                    if (countryName.equalsIgnoreCase(GeneralData.FETCH_ADDRESS_COUNTRY_SAUDI)) {
                        if (GeneralData.getListPointsSize() == 1)
                            GeneralData.addToListPoints(latLngTarget, locationName);
                        else
                            GeneralData.addToListPoints(1, latLngTarget, locationName);

                        ifSaudi[1] = true;
                    } else {
                        ifSaudi[1] = false; // destination not Saudi
                    }

                }
            } // end location name != null

        }
    } // end AddressReceiver class






    private String getRequestDirection(String requestURL) throws IOException {
        String responseString = "";

        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;

        try {
            URL url = new URL(requestURL);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            // Get the response result
            inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            responseString = stringBuilder.toString();
            bufferedReader.close();
            inputStreamReader.close();
        } catch (MalformedURLException e) {
            ExceptionHandling.exceptionOccurred("MapsFragment", "MalformedURLException " + e.getMessage());
        } catch (IOException e) {
            ExceptionHandling.exceptionOccurred("MapsFragment", "IOException " + e.getMessage());
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if(httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }

        return  responseString;
    } // end getRequestDirection






    @SuppressLint("StaticFieldLeak")
    public class TaskRequestDirections extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {


            try {
                responseString = getRequestDirection(strings[0]);
            } catch (IOException e) {
                ExceptionHandling.exceptionOccurred("TaskRequestDirections", "IOException " + e.getMessage());
                e.printStackTrace();
            }

            return responseString;
        }


        @Override
        protected void onPostExecute(String response_string) {
            super.onPostExecute(response_string);
            responseString = response_string;

            // Parse JSON here
            TaskParser taskParser = new TaskParser();
            taskParser.execute(response_string);
        }


    } // End TaskRequestDirections class






    @SuppressLint("StaticFieldLeak")
    public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jsonObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jsonObject = new JSONObject(strings[0]);

                DirectionsParser directionsParser = new DirectionsParser();
                routes = directionsParser.parse(jsonObject);
            } catch (JSONException e) {
                ExceptionHandling.exceptionOccurred("TaskParser", "JSONException " + e.getMessage());
            }

            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            // Get list route and disable it into the map

            ArrayList<LatLng> points;
            PolylineOptions polylineOptions = null;
            double lat, lon;

            for (List<HashMap<String, String>> path : lists) {
                points = new ArrayList<>();
                polylineOptions = new PolylineOptions();

                for (HashMap<String, String> point : path) {
                    lat = Double.parseDouble(point.get("lat"));
                    lon = Double.parseDouble(point.get("lon"));

                    points.add(new LatLng(lat, lon));
                } // inner for
                polylineOptions.addAll(points);
                polylineOptions.width(18);
                polylineOptions.color(getResources().getColor(R.color.color_main_orange));
                polylineOptions.geodesic(true);
            } // outer for

            if (polylineOptions != null) {
                mMap.addPolyline(polylineOptions);
            }// else {
            //Toast.makeText(getContext(), "Direction not found!", Toast.LENGTH_LONG).show();
            //}
        }

    } // End Task Parser class





}
