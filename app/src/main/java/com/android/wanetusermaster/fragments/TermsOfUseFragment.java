package com.android.wanetusermaster.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.wanetusermaster.R;


public class TermsOfUseFragment extends Fragment {

    public TermsOfUseFragment() {}

    public static TermsOfUseFragment newInstance() {
        return new TermsOfUseFragment();
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms_of_use, container, false);

        TextView termsContent2 = view.findViewById(R.id.termsContent2);
        termsContent2.setText(Html.fromHtml(
                "1. " + getResources().getString(R.string.terms_content_1)
                        + "<br><br>"
                        + "2. " + getResources().getString(R.string.terms_content_2)
                        + "<br><br>"
                        + "3. " + getResources().getString(R.string.terms_content_3)
        ));

        return view;
    }

}
